rem @echo off

rem mine:「自身の」「宝庫」「鉱坑」「地雷」

rem ファイル・ディレクトリ名設定
set bookutil_exe="YaneuraOu-2017-early-clang-bookutil-sse42.exe"
set evaldir=epoch8
set bookdir=book
set eval_tmp=%evaldir:\=_%
set eval_tmp=%eval_tmp::=_%
set eval_tmp=%eval_tmp:/=_%
set eval_tmp=%eval_tmp:.=_%
set filebase=%bookdir%\%eval_tmp%_self
set sfenfile=%filebase%.sfen
set kif2file=%filebase%.txt
set bookfile=%filebase%.db

rem 定跡追加・局面評価設定
set threads=%NUMBER_OF_PROCESSORS%
set contempt=129
set multipv[1]=600
set thinkopt[1]=moves 4 depth 24
set multipv[2]=10
set thinkopt[2]=moves 16 depth 20
set multipv[3]=5
set thinkopt[3]=moves 24 depth 18

rem 追加局面抽出設定
set gameplylimit=24
set evalblackdiff=30
set evalblacklimit=-0
set evalwhitediff=30
set evalwhitelimit=-140
set depthlimit=0

rem 初回ハッシュ計算
set CompHash=
for /f "usebackq delims=" %%h in (`CertUtil -hashfile "%bookfile%" SHA256 ^| find /v "SHA256" ^| find /v "CertUtil:"`) do set CompHash=%%h
set i=1

rem bookdir作成
if NOT EXIST "%bookdir%" mkdir "%bookdir%"

rem 循環処理始点
:MAINLOOP

if NOT DEFINED multipv[%i%] exit /b 0
if NOT DEFINED thinkopt[%i%] exit /b 0

setlocal ENABLEDELAYEDEXPANSION
set multipv=!multipv[%i%]!
set thinkopt=!thinkopt[%i%]!

set Hash=%CompHash%

rem 現在日時(yyyymmdd_hhmmss)+乱数(_00000 〜 _32767)
set DATE_tmp=%DATE: =0%
set TIME_tmp=%TIME: =0%
set TIME_tmp=%TIME_tmp::=%
set /a RND_tmp=100000+%RANDOM%
set DTR=%DATE_tmp:/=%_%TIME_tmp:~0,6%_%RND_tmp:~-5%

rem 一時ファイル名
set kif2tmp=%filebase%_%DTR%.txt
set sfentmp=%filebase%_%DTR%.sfen

rem 定跡拡張
%bookutil_exe% BookFile no_book , EvalDir %evaldir% , Threads %threads% , MultiPV %multipv% , Contempt %contempt% , bookutil to_kif2 %bookfile% %kif2tmp% moves %gameplylimit% evalblackdiff %evalblackdiff% evalwhitediff %evalwhitediff% evalblacklimit %evalblacklimit% evalwhitelimit %evalwhitelimit% opening_ply2all depthlimit %depthlimit% comment , bookutil to_sfen %bookfile% %sfentmp% moves %gameplylimit% evalblackdiff %evalblackdiff% evalwhitediff %evalwhitediff% evalblacklimit %evalblacklimit% evalwhitelimit %evalwhitelimit% opening_ply2all depthlimit %depthlimit% comment , bookutil think %sfentmp% %bookfile% %thinkopt% , quit

rem 一時ファイルから上書き
move /Y %kif2tmp% %kif2file%
move /Y %sfentmp% %sfenfile%

rem ハッシュ計算
set CompHash=
for /f "usebackq delims=" %%h in (`CertUtil -hashfile "%bookfile%" SHA256 ^| find /v "SHA256" ^| find /v "CertUtil:"`) do set CompHash=%%h

rem ハッシュ一致なら次の段階へ移行
if "%Hash%" == "%CompHash%" set /a i=%i%+1
goto MAINLOOP
